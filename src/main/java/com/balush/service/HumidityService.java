package com.balush.service;

import com.balush.model.Humidity;

import java.util.List;

public interface HumidityService {

    Humidity save(Humidity humidity);

    List<Humidity> findAll();

}
