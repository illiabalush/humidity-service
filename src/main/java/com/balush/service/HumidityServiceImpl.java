package com.balush.service;

import com.balush.model.Humidity;
import com.balush.repository.HumidityRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HumidityServiceImpl implements HumidityService {

    private HumidityRepository humidityRepository;

    public HumidityServiceImpl(HumidityRepository humidityRepository) {
        this.humidityRepository = humidityRepository;
    }

    @Override
    public Humidity save(Humidity humidity) {
        return humidityRepository.save(humidity);
    }

    @Override
    public List<Humidity> findAll() {
        return humidityRepository.findAll();
    }
}
