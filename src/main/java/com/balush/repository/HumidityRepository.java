package com.balush.repository;

import com.balush.model.Humidity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HumidityRepository extends MongoRepository<Humidity, Long> {
}
