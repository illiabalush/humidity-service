package com.balush.model;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Document(collection = "humidity")
public class Humidity {

    private Float value;

    private Coordinates coordinates;

    private long timestamp;
}
