package com.balush.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Data implements Serializable {

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private Float value;

    @JsonProperty("coordinates")
    private Coordinates coordinates;

    @JsonProperty("timestamp")
    private long timestamp;

}
