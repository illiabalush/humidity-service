package com.balush.pipeline.process;

import com.balush.model.Data;
import com.balush.model.Humidity;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class DataMappingProcess implements Process<Data, Humidity> {

    @Override
    public Humidity process(Data input) {
        log.info("Mapping Data -> Humidity");
        return Humidity.builder()
                .coordinates(input.getCoordinates())
                .value(input.getValue())
                .timestamp(input.getTimestamp())
                .build();
    }

}
