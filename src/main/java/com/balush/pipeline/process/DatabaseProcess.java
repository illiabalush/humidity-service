package com.balush.pipeline.process;

import com.balush.model.Humidity;
import com.balush.service.HumidityService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@AllArgsConstructor
public class DatabaseProcess implements Process<Humidity, Humidity> {

    private final HumidityService humidityService;

    @Override
    public Humidity process(Humidity input) {
        log.info("Saving " + input.toString() + " to db");
        return humidityService.save(input);
    }
}
