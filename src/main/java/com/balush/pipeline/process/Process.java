package com.balush.pipeline.process;

public interface Process<I, O> {
    O process(I input);
}
