package com.balush.pipeline;

import com.balush.pipeline.process.Process;

public class Pipeline<I, O> {

    private final Process<I, O> current;

    public Pipeline(Process<I, O> current) {
        this.current = current;
    }

    public  <NewO> Pipeline<I, NewO> pipe(Process<O, NewO> next) {
        return new Pipeline<>(input -> next.process(current.process(input)));
    }

    public O execute(I input) {
        return current.process(input);
    }

}