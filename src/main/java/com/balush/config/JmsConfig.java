package com.balush.config;

import com.rabbitmq.jms.admin.RMQConnectionFactory;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.ConnectionFactory;


@Log4j2
@Configuration
@AllArgsConstructor
public class JmsConfig {

    private final RabbitProperties rabbitProperties;

    @Bean
    public ConnectionFactory connectionFactory() {
        RMQConnectionFactory rmqConnectionFactory = new RMQConnectionFactory();
        rmqConnectionFactory.setHost(rabbitProperties.getHost());
        rmqConnectionFactory.setPort(rabbitProperties.getPort());
        return rmqConnectionFactory;
    }

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

}
