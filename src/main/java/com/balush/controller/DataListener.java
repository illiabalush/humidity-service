package com.balush.controller;

import com.balush.model.Data;
import com.balush.model.Humidity;
import com.balush.pipeline.Pipeline;
import com.balush.pipeline.process.DataMappingProcess;
import com.balush.pipeline.process.DatabaseProcess;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Log4j2
@RequiredArgsConstructor
public class DataListener {

    private Pipeline<Data, Humidity> pipeline;
    private final DataMappingProcess dataMappingProcess;
    private final DatabaseProcess databaseProcess;

    @PostConstruct
    private void init() {
        pipeline = new Pipeline<>(dataMappingProcess).pipe(databaseProcess);
    }

    @JmsListener(destination = "humidity-data-queue")
    public void listenData(Data data) {
        log.info("Humidity listener receive: " + data);
        pipeline.execute(data);
    }
}
